package com.weekendservice.weekenddetectorapi;

import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class WeekenddetectorService {

    public boolean isItWeekendAlready(String date) { // "2018-04-18 17:30"
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(date, formatter);

        if (dateTime.getDayOfWeek() == DayOfWeek.TUESDAY ||
                dateTime.getDayOfWeek() == DayOfWeek.WEDNESDAY ||
                dateTime.getDayOfWeek() == DayOfWeek.THURSDAY) {
            return false;
        } else if (dateTime.getDayOfWeek() == DayOfWeek.SATURDAY ||
                dateTime.getDayOfWeek() == DayOfWeek.SUNDAY) {
            return true;
        } else if (dateTime.getDayOfWeek() == DayOfWeek.MONDAY) {
            if (dateTime.getHour() < 6) {
                return true;
            } else {
                return false;
            }
        } else { // Reede
            if (dateTime.getHour() < 16) {
                return false;
            } else {
                return true;
            }
        }
    }
}
