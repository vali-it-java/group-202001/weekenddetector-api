package com.weekendservice.weekenddetectorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WeekenddetectorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeekenddetectorApiApplication.class, args);
	}

}
