package com.weekendservice.weekenddetectorapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/weekenddetector")
@CrossOrigin("*")
public class WeekenddetectorController {

    @Autowired
    private WeekenddetectorService weekenddetectorService;

    @PostMapping
    @GetMapping
    public boolean isItWeekendAlready(@RequestParam String date) {
        // Siia sisse tuleb äriloogika.
        return weekenddetectorService.isItWeekendAlready(date);
    }
}
