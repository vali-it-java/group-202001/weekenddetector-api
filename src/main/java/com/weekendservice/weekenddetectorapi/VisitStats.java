package com.weekendservice.weekenddetectorapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/visits")
@CrossOrigin("*")
public class VisitStats {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/all_the_stats_available_in_db")
    public List<VisitStatEntity> getVisitStats() {
        String sql = "select * from visit";
        List<VisitStatEntity> entities = jdbcTemplate.query(
                sql, // Sisendparameeter 1

                // Sisendparameeter 2
                // Instruktsioon, kuidas data reast teha meile sobiv objekt.
                (rowData, rowNumber) -> {
                    // Paneme kokku oma objekti, kasutades andmeid dataRod objektist
                    VisitStatEntity x = new VisitStatEntity();
                    x.id = rowData.getInt("id");
                    x.visitEventTime = rowData.getString("visit_time");
                    x.someOtherPropety = "See on ka mingi property " + rowNumber;
                    return x;
                }
        );
        return entities;
    }

    @GetMapping("/register_event_denoting_user_visit")
    public void registerVisit() {
        String currentTime = LocalDateTime.now().toString();
//        String sql = "insert into visit (visit_time) values ('" + currentTime + "')";
//        jdbcTemplate.update(sql);

        String sql = "insert into visit (visit_time) values (?)";
        jdbcTemplate.update(sql, currentTime);
    }

    @GetMapping("/give_me_visit_count")
    public int getVistiCount() {
        // Siin oleks tegelikult ilus teha select count(*) ... päring otse andmebaasi!
        return getVisitStats().size();
    }

    public static class VisitStatEntity {
        public int id;
        public String visitEventTime;
        public String someOtherPropety;
    }
}
