package com.weekendservice.weekenddetectorapi;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
public class WeekenddetectorServiceTest {

    @Autowired
    private WeekenddetectorService weekenddetectorService;

    @Test
    public void testIsItWeekendAlready() {
        boolean isWeekend =
                weekenddetectorService.isItWeekendAlready("2020-02-09 11:00");
        Assert.isTrue(isWeekend, "Weekend not detected!");

        isWeekend =
                weekenddetectorService.isItWeekendAlready("2020-02-10 11:00");
        Assert.isTrue(!isWeekend, "Weekend detected incorrectly!");

        isWeekend =
                weekenddetectorService.isItWeekendAlready("2020-02-10 05:00");
        Assert.isTrue(isWeekend, "Weekend not detected!");

        isWeekend =
                weekenddetectorService.isItWeekendAlready("2020-02-07 15:59");
        Assert.isTrue(!isWeekend, "Weekend detected incorrectly!");

        isWeekend =
                weekenddetectorService.isItWeekendAlready("2020-02-07 16:01");
        Assert.isTrue(isWeekend, "Weekend not detected!");
    }
}
